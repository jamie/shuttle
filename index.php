<?php 

/**
 * This code should be the end point for your Twilio Messaging web hook.
 * It ensures that an SMS messages sent gets converted into an email.
*/

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

$shtl = new Shuttle();
$shtl->main();

class Shuttle {
  private $email;
  private $addressBook;
  private $logFile;
  private $robotEmailPrefix;
  private $robotEmailDomain;
  private $smtpHost;
  private $smtpUser;
  private $smtpPass;

  public function __construct() {
    $configFile = getenv('SHUTTLE_CONFIG', NULL);
    if (!$configFile) {
      $configFile = '../include/shuttle.yml';
    }
    if (!file_exists($configFile)) {
      trigger_error("The config file ($configFile) could not be found.");
    }

    $config = yaml_parse_file($configFile);
    $this->email = $config['human_email'];
    $this->twilioNumber = $config['twilio_number'];
    $this->addressBook = $config['addressbook'];
    $this->logFile = $config['log_dir'] . '/incoming.log';
    $this->robotEmailPrefix = $config['robot_email_prefix'];
    $this->robotEmailDomain = $config['robot_email_domain'];
    $this->smtpHost = $config['smtp_host'];
    $this->smtpUser = $config['smtp_user'];
    $this->smtpPass = $config['smtp_pass'];

  }

  public function main() {
    $msg = '';
    if (!$this->validate() || !$this->send()) {
      $msg = 'Errors. Check log.';
    }
    $this->outputHeader($msg);
  }

  private function logMsg($msg) {
    $msg = date('Y-m-d H:i:s') . ' ' . $msg . "\n";
    file_put_contents($this->logFile, $msg, FILE_APPEND);
  }

  private function outputHeader($msg = '') {
    header('Content-type: text/xml');
    echo '<?xml version="1.0" encoding="UTF-8"?>';
    echo "<Response>{$msg}</Response>"; 
  }

     
  private function validate() {
    // Don't send empty email messages.
    $required = [ 'From', 'To', 'Body' ];
    foreach($required as $req) {
      $value = $_REQUEST[$req] ?? NULL;
      if (!$value) {
        // Fail
        $this->logMsg("Missing a required field: '$req', REQUEST dump follows");
        $this->logMsg(print_r($_REQUEST, TRUE));
        return FALSE;
      }
    }
    if ('+' . $this->twilioNumber != $_REQUEST['To']) {
      $this->logMsg("Not sent to our SMS number, sent to {$_REQUEST['To']}");
      return FALSE;
    }
    if (!preg_match('/^\+1[0-9]{10}$/', $_REQUEST['From'])) {
      $this->logMsg("Invalid from address: {$_REQUEST['From']}");
      return FALSE;
    }
    return TRUE;
  }

  private function send() {
    // The From key is the cell phone number (e.g. +19171238900)
    $number = $_REQUEST['From'];

    // The body is the sms message itself.
    $body = nl2br($_REQUEST['Body']);

    // If the name matches an address book, use the friendly address
    // book name.
    $fromName = $this->addressBook[$number] ?? 'Unknown';
    // Create a from address that, if replied to, will result in an sms message
    // being sent. The $number variable should start with a plus sign.
    $fromEmail = $this->robotEmailPrefix . $number . '@' . $this->robotEmailDomain;

    $subject = $body; 
    $message = "<p>The message is from {$number} ({$fromName}). Put your reply in the subject line, not here in the body.</p>\n";
    return $this->sendViaPhpMailer($subject, $message, $fromEmail, $fromName);
  }

  private function sendViaPhpMailer($subject, $message, $fromEmail, $fromName) {
    $mail = new PHPMailer(true);
    try {
        // Server settings
        $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->Host = $this->smtpHost; 
        $mail->SMTPAuth = true;
        $mail->Username = $this->smtpUser; 
        $mail->Password = $this->smtpPass; 
        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;

        // Recipients
        $mail->setFrom($fromEmail, $fromName);
        $mail->addAddress($this->email);

        // Content
        $mail->isHTML(true);
        $mail->Subject = $subject;
        $mail->Body    = $message;
        $mail->AltBody = strip_tags($message);

        $mail->send();
        $this->logMsg("Send message from {$fromEmail}");
    } catch (Exception $e) {
        $this->logMsg("Message could not be sent. Mailer Error: {$mail->ErrorInfo}");
        return FALSE;
    }
    return TRUE;
  }
}
