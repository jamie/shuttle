# Shuttle uses two email addresses. One is the "human" who is sending email
# messages that should be converted to SMS messages and also the address that
# any errors trying to convert email messages into SMS messages will be sent.
#
# The second email address is the robot address that messages you want
# converted into SMS should be sent. This address should support the +
# modifier, so messages sent to, for example, sms+1234567890@example.net get
# delivered to the sms@example.net mailbox regardless of the numbers following
# the plus sign.

# This is the human email address. We will send received SMS messages
# to this address as well as errors that we encounter when trying to 
# relay SMS messages we received via email. We also expect 
human_email: me@example.net 

# This is the robot address. When sending messages via SMS that have been
# delivered to our mailbox, we we have to parse the TO address to pull out the
# right phone number to use. This is how to parse the To address to pull out
# the phone number. We expect email sent to
# {to_email_prefix}+12345678901@{to_email_domain}
robot_email_prefix: sms
robot_email_domain: example.net 

# This is the IMAP credentials for the robot email address. Retrieve messages
# to send via SMS using this IMAP host with these credentials. Any email
# (matching the right criteria) that ends up in this mailbox will be sent out
# via SMS.
imap_host: mail.example.net
imap_user: sms
imap_pass: xxxxxxxxxxx 

# This is the SMTP credentials for the robot account. It might be the same as
# the IMAP credentials.
#
# When a message is sent to us via SMS, we will forward it to the designated
# email address using the login info below. This info will also be used to
# relay errors we encounter when trying to send SMS messages.
smtp_host: mail.example.net
smtp_user: sms
smtp_pass: xxxxxxx 

# When converting an email messages to SMS, If a received line does not match
# this domain, refuse to send. This ensures we only forward if the message is
# sent via our mail system, not an external system.
received_root_domain: example.net 
received_last_domain: mail.example.net

# We will create outgoing.log (for converting messages from email to SMS),
# incoming log (for converting SMS messages to email) and an optin.log (for
# processing optin requests). All logs will be create in this directory.
log_dir: ../include

# Set log level to debug, info, error, warning, or critical
log_level: debug

twilio_number: 19995554444
twilio_account_sid: xxxxxxxxxxxxxxxxx 
twilio_auth_token: xxxxxxxxxxxxxx 

# Name listed on twilio opt in form.
app_name: My Cool App 

# Address book - for incoming SMS messages allows the email address
# to have a recongizable name in the FROM field.
addressbook:
  '+12345678900': 'Julia'
  '+12342342424': 'Jose'

