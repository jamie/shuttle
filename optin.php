<?php

/**
 * Twilio requires an opt-in form to get a 10DLC
 * registration, so here it is.
 */

$config_file = getenv('SHUTTLE_CONFIG', NULL);
if (!$config_file) {
  $config_file = '../include/shuttle.yml';
}
if (!file_exists($config_file)) {
  trigger_error("The config file ($config_file) could not be found.");
}

$config = yaml_parse_file($config_file);
$app_name = $config['app_name'];
$optin_log = $config['log_dir'] . '/optin.log';

// Is their a submission to process?
$consent = $_POST['consent'] ?? NULL;
$name = $_POST['name'] ?? NULL;
$number = $_POST['number'] ?? NULL;
$message = 'Please enter your name and phone number below to opt in.';
$message_class = "info";
if ($_POST) {
  if (!$consent) {
    $message = "Please check the consent checkbox";
    $message_class = "error";
  }
  else if (!$name) {
    $message = "Please enter your name";
    $message_class = "error";
  }
  else if (!$number) {
    $message = "Please enter your number";
    $message_class = "error";
  }
  else if (!preg_match('/^[0-9]{10}$/', $number)) {
    $message = "Please only enter 10 digits for your phone number.";
    $message_class = "error";
  }
  else {
    $date = date('Y-m-d H:i:s');
    file_put_contents($optin_log, "{$date}: {$name} opted in using {$number}\n", FILE_APPEND);
    $message = "You have been opted in. You may opt out at any time by replying with STOP.";
    $message_class = "success";
  }
}
?>

<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Choose your webmail software!</title>
    <style>
      body {
        font-family: sans-serif;
        font-size: 14px;
        line-height: 1.4;
        margin: 30px;
      }
      button,
      input,
      select,
      textarea {
        font-family: inherit;
        font-size: 100%;
        width: 150px;
        padding: 0;
        margin: 0;
        box-sizing: border-box;
      }
      .table {
        display: table;
      }
      .row {
        display: table-row;
      }
      .cell {
        display: table-cell;
        padding-bottom: 5px;
      }
      label {
        font-weight: bold;
      }
      .disclaimer {
        font-style: italic;
        margin-top: 5px;
      }
      p.message {
        padding: 5px;
      }
      p.info {
        color: #004085;
        background-color: #cce5ff;
        border-color: #b8daff;
      }
      p.error {
        color: #856404;
        background-color: #fff3cd;
        border-color: #ffeeba;
      }
      p.success {
        color: #155724;
        background-color: #d4edda;
        border-color: #c3e6cb;
      }
      #container {
        max-width: 600px;
      }
    </style>
  </head>

  <div id="container">
    <h1>Welcome</h1>

    <h3>The <?php echo $app_name ?> SMS application</h3>

    <p class="message <?php echo $message_class; ?>"><?php echo $message;?></p>

    <form method="post">
      <div class="table">
        <div id="from" class="row">
          <div class="cell"><label for="name">Name:</label></div>
          <div class="cell"><input type="text" id="name" name="name" value="<?php echo $name; ?>"/></div>
        </div>
        <div id="number" class="row">
          <div class="cell"><label for="number">Mobile number:</label></div>
          <div class="cell"><input type="tel" id="number" name="number" value="<?php echo $number ?>" /></div>
        </div>
        <div id="consent" class="row">
          <div class="cell"><label for="consent">Sign up for SMS:</label></div>
          <div class="cell"><input type="checkbox" id="consent" name="consent" <?php if ($consent) echo "checked"; ?>  /></div>
        </div>
        <div id="consent" class="row">
        </div>
        <div id="submit" class="row">
          <div class="cell"><input type="submit" value="Submit"></div>
        </div>
      </div>
      <div class="disclaimer">By submitting this form you agree to receive SMS messages. The terms and privacy policy can be found <a href="privacy.html">here</a>. You may receive up to 5 messages per week. Text and data rates may apply. Reply STOP to end or HELP for help.</div>
    </form>
  </div>
</body>
</html>

