#!/usr/bin/python3

import imaplib2
import smtplib
from threading import *
from email import message_from_string
from email.header import decode_header
from email.utils import parseaddr
from email.message import EmailMessage
import os
import sys
import re
import time
import yaml
import logging
from twilio.rest import Client

# Largely based on https://gist.github.com/thomaswieland/3cac92843896040b11c4635f7bf61cfb

# The Shuttle class handles managing the IMAP IDLE command that
# triggers every time a new message arrives.
class Shuttle(object):
    config = {}
    # The imaplib2 object.
    M = None
    event = None
    thread = None
    needsync = False
    logger = None

    def __init__(self, conn, config):
        self.thread = Thread(target=self.idle)
        self.M = conn
        self.event = Event()
        self.config = config

    # Define the required thread methods.
    def start(self):
        self.thread.start()
 
    def stop(self):
        # Setting the event will trigger the loop to stop. 
        self.event.set()
 
    def join(self):
        self.thread.join()
 
    # The method that gets called when a new email arrives. 
    def process_unseen_messages(self):
        typ, data = self.M.search(None, '(UNSEEN)')
        for num in data[0].split():
            typ, email_data = self.M.fetch(num, '(RFC822)')
            raw_email = email_data[0][1].decode('utf-8')
            message = message_from_string(raw_email)
            self.process_message(message)

    # Send email to our configured from address to notify of an event
    # or error.
    def notify(self, message):
        logging.info(f"Notifying about {message}")
        body = None
        if len(message) > 50:
            body = message
            message = "SMS status message"
        msg = EmailMessage()
        msg['Subject'] = message.strip()
        msg['From'] = f'"Shuttle SMS Service" <{self.config["human_email"]}>' 
        msg['To'] = self.config['human_email'] 
        if body:
            msg.set_content(body)

        try:
            with smtplib.SMTP(self.config['smtp_host'], 587) as smtp:
                smtp.ehlo()
                # We have to create our own ssl context to ensure certificate checking is enforced.
                smtp.starttls()
                smtp.ehlo()
                smtp.login(self.config['smtp_user'], self.config['smtp_pass'])
                smtp.send_message(msg)
                logging.info(f"Email sent to {self.config['human_email']}")
        except Exception as err:
            logging.error(f"Failed to send email log message: {err}")

    def log_and_notify(self, message, level):
        if level == logging.DEBUG:
            logging.debug(message)
        elif level == logging.INFO:
            logging.info(message)
        elif level == logging.WARNING:
            logging.warning(message)
        elif level == logging.ERROR:
            logging.error(message)
        elif level == logging.CRITICAL:
            logging.critical(message)
        else:
            raise RuntimeException(f"Unknown log level: {level}")

        self.notify(message)

    def get_header(self, message, header):
        value = message.get(header, "")
        if value:
            if header == "subject":
                # Decode subject
                subject = decode_header(value)
                if subject[0][1]:
                    value = subject[0][0].decode(subject[0][1])
                else:
                    value = subject[0][0]
            elif header == 'from' or header == 'to':
                # Extract just the email portion of a "Name" <email> line.
                line = parseaddr(value)
                value = line[1]
        return value 

    def extract_sms_number(self, email):
        to_email_pattern = f"{self.config['robot_email_prefix']}" + r'\+(1[0-9]{10})@' + f"{self.config['robot_email_domain']}"

        match = re.match(to_email_pattern, email)
        if not match:
          logging.warngin(f"Failed to extract sms number from {email}")
          return False
        return match.group(1)

    def authenticate_message(self, message):
        from_email = self.get_header(message, "from")
        if from_email != self.config['human_email']:
          self.log_and_notify(f"Invalid From address {from_email}", logging.WARNING)
          return False

        to_email = self.get_header(message, "to")
        if self.extract_sms_number(to_email) is False:
            self.log_and_notify(f"Failed to extract phone number from {to_email}.", logging.WARNING)
            return False
        
        subject = self.get_header(message, "subject")
        if not subject:
            self.log_and_notify(f"Empty subject.", logging.WARNING)
            return False
        received_domain_pattern = r'^from ([a-z0-9]+\.' + re.escape(config['received_root_domain']) + ')'
        received_headers = message.get_all('Received')
        last_received_header = ""
        if received_headers:
            for raw_header in received_headers:
              header = raw_header.replace("\r\n", " ").replace("\n", " ")
              match = re.match(received_domain_pattern, header)
              if not match:
                self.log_and_notify(f"Invalid recieved from header: {header}", logging.WARNING)
                return False
              last_received_header = match.group(1) 
        if last_received_header != self.config['received_last_domain']:
          self.log_and_notify(f"Last header should be from {self.received_last_domain} but instead was {last_header}", logging.WARNING)
          return False

        return True

    def send_sms(self, sms_number, subject):
        # Find your Account SID and Auth Token at twilio.com/console
        # and set the environment variables. See http://twil.io/secure
        account_sid = self.config['twilio_account_sid']
        auth_token = self.config['twilio_auth_token']
        client = Client(account_sid, auth_token)

        try:
            message = client.messages.create(
                body=subject,
                from_=f'+{self.config["twilio_number"]}',
                to=f'+{sms_number}'
            )
        except Exception as exc:
            self.log_and_notify(f"Failed to send SMS to {sms_number}, exception: {exc}", logging.ERROR)
            return False
        self.log_and_notify(f"Successfully sent message to {sms_number}", logging.INFO)
        
    def process_message(self, message):
        if self.authenticate_message(message):
            subject = self.get_header(message, 'subject')
            to = self.get_header(message, 'to')
            sms_number = self.extract_sms_number(to)
            self.send_sms(sms_number, subject)

    def idle(self):
        # Starting an unending loop here
        logging.info("Starting the idle method.")
        while not self.event.is_set():
            logging.debug("Setting needsync to False.")
            self.needsync = False
            # A callback method that gets called when a new 
            # email arrives.
            def callback(args):
                logging.debug(f"Callback is called with {args}.")
                if not self.event.is_set():
                    logging.debug("Event is not set, setting needsync to True.")
                    self.needsync = True
                    self.event.set()
            # Do the actual idle call. This returns immediately, 
            # since it's asynchronous.
            logging.debug("Starting to idle.")
            self.M.idle(callback=callback)
            # This waits until the event is set. The event is 
            # set by the callback, when the server 'answers' 
            # the idle call and the callback function gets 
            # called.
            logging.debug("Setting event wait.")
            self.event.wait()
            # Because the function sets the needsync variable,
            # this helps escape the loop without doing 
            # anything if the stop() is called.
            if self.needsync:
                logging.debug("needsync is True, clearing event, calling process_unseen_messages.")
                self.event.clear()
                self.process_unseen_messages()

def setup_logging(config):
    if config['log_level'] == 'debug':
        level = logging.DEBUG
    elif config['log_level'] == 'info':
        level = logging.INFO
    elif config['log_level'] == 'error':
        level = logging.ERROR
    elif config['log_level'] == 'warning':
        level = logging.WARNING
    elif config['log_level'] == 'critical':
        level = logging.CRITICAL

    logging.basicConfig(filename=f"{config['log_dir']}/outgoing.log",
        level=level, 
        format='%(asctime)s [%(levelname)s]: %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S'
    )

    # If you also want to log the same messages to console (stdout), add a StreamHandler:
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(logging.Formatter('%(asctime)s [%(levelname)s]: %(message)s'))
    logging.getLogger().addHandler(console_handler)

# Start program logic.
if __name__ == "__main__":
    config = None
   
    if len(sys.argv) < 2:
        print("Please provide a path to a YAML configuration file as the first argument.")
        sys.exit(1)

    yaml_path = sys.argv[1]
    if not os.path.exists(yaml_path):
        print(f"The path to your yaml file ({yaml_path}) could not be found.")
    else:
      with open(yaml_path, 'r') as file:
        try:
            config = yaml.safe_load(file)
            setup_logging(config)
            config_copy = config.copy()
            config_copy['imap_pass'] = 'xxxxxxxxxxxxxxxxx'
            config_copy['smtp_pass'] = 'xxxxxxxxxxxxxxxxx'
            logging.debug(f"Loading configuration file: {config_copy}")
        except yaml.YAMLError as exc:
            print(f"Error parsing YAML file {yaml_path}: {exc}")
            config = None

    if config:
        logging.info("Starting shuttle")
        shtl = None
        M = None
        try:
            # Login to IMAP server.
            M = imaplib2.IMAP4_SSL(config['imap_host'])
            M.login(config['imap_user'], config['imap_pass'])
            M.select('INBOX')
                
            # Start monitoring for new email.
            shtl = Shuttle(M, config)
            shtl.start()
            while True:
                time.sleep(10)
        except KeyboardInterrupt:
            logging.warning("Ctrl-c was pressed.")
        finally:
            logging.debug("Shutting down.")
            if shtl:
                shtl.stop()
                shtl.join()
            if M:
                M.close()
        M.logout()
